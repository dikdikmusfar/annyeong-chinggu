# vuecli-jabarcoding-final-project

### List Anggota Kelompok 2
- Ajeng Aisyah Rahayu
- Dikdik Musfar
- Tiara Yasmine Soraya

### THIS SITE IS NOW LIVE!

```
https://quizzical-lalande-98d294.netlify.app/
```

### VIDEO DEMO

```
comming soon
```

### SCREENSHOTS

```
https://drive.google.com/drive/folders/1JTkeqOfL43U1JgZEC4_Rh873rQgiga0i?usp=sharing
```

### Project setup

```
npm install
npm install vee-validate
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

### Disclaimer

```
This project was made as a final project in Jabar Coding Camp - VueJS 2021 

```
