import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';
import colors from 'vuetify/lib/util/colors'

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        dark:false,
        themes: {
          light: {
            primary: colors.amber.lighten1,
            secondary: colors.red.darken1,
            anchor: colors.blue.darken1,
            background: '#000000'
          },
        },
      }
});